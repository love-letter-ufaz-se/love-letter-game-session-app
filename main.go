package main

import (
	"context"
	"errors"
	"log"
	"math/rand"
	"net"
	"strconv"
	"strings"
	"time"

	"google.golang.org/grpc"

	"github.com/go-redis/redis/v7"
	"gitlab.com/love-letter-ufaz-se/love-letter-game-session-app/v2/redismngr"
	"gitlab.com/love-letter-ufaz-se/love-letter-game-session-app/v2/session"
)

type server struct{}

var redisClient *redis.Client

func main() {
	redisClient = redis.NewClient(&redis.Options{
		Addr:     "redis:6379",
		Password: "",
		DB:       0,
	})
	defer redisClient.Close()

	_, err := redisClient.Ping().Result()
	handleErr("Can't ping redis server", err)

	address := "0.0.0.0:50051"
	lis, err := net.Listen("tcp", address)
	handleErr("Error while starting listen server for main", err)
	s := grpc.NewServer()
	session.RegisterSessionServiceServer(s, &server{})
	log.Println("Starting serving")
	s.Serve(lis)
	log.Println("Started serving")
}

func (*server) CheckSession(ctx context.Context, request *session.SessionCookie) (*session.SessionResponse, error) {
	cookie, err := redisClient.Get("session:" + request.Cookie).Result()
	if err == redis.Nil {
		response := &session.SessionResponse{
			Session: "guest",
		}
		return response, nil
	} else if err != nil {
		return nil, errors.New("Redis got error")
	}
	response := &session.SessionResponse{
		Session: cookie,
	}
	return response, nil
}

func (*server) NewGuestSession(ctx context.Context, request *session.NoParams) (*session.NewSession, error) {
	newToken, err := generateSessionToken()
	handleErr("", err)
	expTime, _ := time.ParseDuration("24h")
	err = redisClient.Set("session:"+newToken, "guest", expTime).Err()
	handleErr("Error while setting redis key for New Guest Session", err)
	uniqueID, err := generateUniqueID()
	handleErr("Error while setting generating Unique ID for NewGuestSession", err)
	err = redisClient.ZAddCh("session_unique_id", &redis.Z{
		Member: newToken,
		Score:  float64(uniqueID),
	}).Err()
	handleErr("Error while setting Unique ID for New Guest Session", err)
	setExpirationofSortedSetMember("session_unique_id", newToken, "24h")

	response := &session.NewSession{
		NewCookie: newToken,
	}
	return response, err
}

func (*server) NewUserSession(ctx context.Context, request *session.UserID) (*session.NewSession, error) {
	newToken, err := generateSessionToken()
	handleErr("", err)
	expTime, _ := time.ParseDuration("24h")
	err = redisClient.Set("session:"+newToken, request.ID, expTime).Err()
	handleErr("Error while setting redis key for New User Session", err)
	uniqueID, err := generateUniqueID()
	handleErr("Error while setting generating Unique ID for NewGuestSession", err)
	err = redisClient.ZAddCh("session_unique_id", &redis.Z{
		Member: newToken,
		Score:  float64(uniqueID),
	}).Err()
	handleErr("Error while setting Unique ID for New Guest Session", err)
	setExpirationofSortedSetMember("session_unique_id", newToken, "24h")
	response := &session.NewSession{
		NewCookie: newToken,
	}
	return response, err
}

func (*server) GetUniqueID(ctx context.Context, request *session.SessionCookie) (*session.SessionResponse, error) {
	uniqueID, err := redisClient.ZScore("session_unique_id", request.Cookie).Result()
	resp := &session.SessionResponse{
		Session: strconv.Itoa(int(uniqueID)),
	}
	return resp, err

}

func handleErr(msg string, err error) {
	if err != nil {
		log.Fatalf("%s ... %v\n", msg, err)
	}
}

func generateSessionToken() (string, error) {
	if redisClient == nil {
		log.Fatalln("redisClient is nil!")
		return "", nil
	}
	digits := "0123456789"
	specials := "~=+%^*/()[]{}/!@#$?|"
	rand.Seed(time.Now().UnixNano())
	chars := []rune("ABCDEFGHIJKLMNOPQRSTUVWXYZ" +
		"abcdefghijklmnopqrstuvwxyz" +
		digits + specials)
	length := 16
	var b strings.Builder
	for i := 0; i < length; i++ {
		b.WriteRune(chars[rand.Intn(len(chars))])
	}
	str := b.String()
	_, err := redisClient.Get("session:" + str).Result()
	if err != redis.Nil {
		return generateSessionToken()
	}

	return str, nil
}

func generateUniqueID() (int32, error) {
	if redisClient == nil {
		log.Fatalln("redisClient is nil!")
		return -1, nil
	}
	rand.Seed(time.Now().UnixNano())
	uniqueID := rand.Int31()
	cnt, err := redisClient.ZCount("session_unique_id", strconv.Itoa(int(uniqueID)), strconv.Itoa(int(uniqueID))).Result()

	if err != nil || cnt != 0 {
		return generateUniqueID()
	}

	return uniqueID, nil
}

func setExpirationofSortedSetMember(key string, val string, exp string) error {
	opts := grpc.WithInsecure()
	redismngrGRPC, err := grpc.Dial("redismngr-app:50051", opts)
	handleErr("Could not create connection between redismngr-app", err)
	defer redismngrGRPC.Close()

	client := redismngr.NewRedisMngrServiceClient(redismngrGRPC)

	request := &redismngr.RedisData{
		Key:          key,
		ElementValue: val,
		Expires:      exp,
	}

	_, err = client.NewSortedSetMember(context.Background(), request)

	return err
}
