module gitlab.com/love-letter-ufaz-se/love-letter-game-session-app/v2

go 1.14

require (
	github.com/go-redis/redis/v7 v7.2.0
	github.com/golang/protobuf v1.4.0
	google.golang.org/grpc v1.29.0
)
